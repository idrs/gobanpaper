class Property {
  String ident;
  String[] values;
  
  Property(String id,String[] val){
    ident = id;
    values = val;
  }
  String getId(){
    return ident;
  }
  String[] getVal(){
    return values;
  }
  String toString(){
    String out = ident+":";
    for(int i=0;i<values.length;i++){
      out += values[i]+"/";
    }
    return out;
  }
}

class Node {
  ArrayList<Property> props;
  ArrayList<Node> nexts;
  Node(){
    props = new ArrayList<Property>();
    nexts = new ArrayList<Node>();
  }
  Node(ArrayList<Property> properties){
    props = properties;
    nexts = new ArrayList<Node>();
  }
  ArrayList<Property> getProps(){
    return props;
  }
  ArrayList<Node> getNexts(){
    return nexts;
  }
  void addProp(Property prop){
    props.add(prop);
  }
  void addNext(Node n){
    Node tmp = new Node(n.getProps());
    ArrayList<Node> nnexts = n.getNexts();
    for(int i=0;i<nnexts.size();i++){
      tmp.addNext(nnexts.get(i));
    }
    nexts.add(tmp);
  }
  void addNexts(ArrayList<Node> nnexts){
    nexts = nnexts;
  }
  Boolean isNexts(){
    return (nexts.size()>0);
  }
  Node getFirstNext(){
    return nexts.get(0);
  }

  void disp(String indent){
    for(int i = 0;i<props.size();i++){
      print(indent);
      println(props.get(i).toString());
    }
    for(int j=0;j<nexts.size();j++){
      nexts.get(j).disp(indent+"  ");
    }
  }

  
}


Node parse2(String sgf){
    int paren = 0;
    String mainline = "";
    int nbVar = 0;
    int startp=-1;
    int stopp;
    StringList variations = new StringList();
    ArrayList<Node> variationNodes = new ArrayList<Node>();
    Node output = new Node();
    for(int i=0;i<sgf.length();i++){
      if(sgf.charAt(i)=='['){
        //print(sgf+" : "+str(i)+"/"+sgf.charAt(i));
        while(sgf.charAt(i)!=']'){i++;}
        //println(" et "+str(i)+"/"+sgf.charAt(i));

      }
      if(sgf.charAt(i) == '(') {
        if(nbVar == 0){
          mainline = sgf.substring(0,i);
          //println(mainline);
        }
        if(paren == 0){
          startp = i;
          nbVar++;
        }  
        paren++;
      }
      if(sgf.charAt(i) == ')'){
        if(paren == 1){
          stopp = i;
          //println("var?:"+sgf.substring(startp+1,stopp));
          variations.append(sgf.substring(startp+1,stopp));
        }
        paren--;
      }
    }
    if(nbVar == 0){mainline = sgf;}
    if(nbVar != variations.size()){
      println("Error in size of variations, found: "+nbVar+" and "+variations.size());
    }
    for(int k = 0;k<variations.size();k++){
      variationNodes.add(parse2(variations.get(k)));
    }
    if(mainline !=""){
      Node tmp = new Node();
      output.addNexts(variationNodes);
      IntList semicolons= new IntList();
      for(int j = 0; j<mainline.length();j++){
        if(mainline.charAt(j)=='['){
          while(mainline.charAt(j)!=']'){
            j++;
          }
        }
        if(mainline.charAt(j) == ';'){
          semicolons.append(j);
        }
      }
      int nbnodes = semicolons.size();
      int end = mainline.length();
      int start;
      String[] nodes = new String[nbnodes];
      for(int j=nbnodes-1;j>=0;j--){ 
        //nodes in reverse order
        start = semicolons.get(j);
        nodes[j] = mainline.substring(start+1,end);
        //println(nodes[j]);
        end = start;
      }
      
      for(int j=nbnodes-1;j>=0;j--){
       String [][] prop = matchAll(nodes[j],"([A-Z]{1,2}?)((?:\\[[^\\]]*?\\])+)");
       String ident;
       String [][] propval;
       //ArrayList<Property> proplist = new ArrayList<Property>();
       Property tmpProp;
       if(prop != null){
       for(int i=0;i<prop.length;i++){
         ident = prop[i][1];
         propval = matchAll(prop[i][2],"\\[([^\\]]*?)\\]");
         String[] values = new String[propval.length];
         for( int k = 0;k<propval.length;k++){
           values[k] = propval[k][1];
         }
         tmpProp = new Property(ident,values);
         //println(tmpProp.toString());
         output.addProp(tmpProp);
       }
       }
       tmp = output;
       output = new Node();
       output.addNext(tmp);
       
      }
    }
   
    else{
     output = new Node();
     output.addNexts(variationNodes);
    }
    return output;
}
