

class Stone{
  public String couleur;
  public float sx;
  public float sy;
  Stone(String c){
    couleur = c;
    sx = fuz * random(-1.0,1.0);
    sy = fuz * random(-1.0,1.0);
       //  println("sx "+ sx+" sy "+ sy);
  }
  void move(float dx, float dy){
    sx +=dx;
    sy +=dy;
  }
  
}

class Board{
  public int size;
  //String boardString;
  Stone[] boardArray;
  Board(int N){
    size = N;
    String boardString = empty(size);
    boardArray = new Stone[(size+2)*(size+2)];
    for(int i = 0;i<boardArray.length;i++){
      switch(boardString.charAt(i)){
        case('\n'):
        case(' '):
          boardArray[i] = new Stone("border");
          break;
        case('.'):
          boardArray[i] = new Stone("empty");
      }
    }     
  }
  Stone get(int i){
    return boardArray[i];
  }
  void add(int i, Stone s){
       //  println(i+": sx "+ s.sx+" & sy "+s.sy);
    boardArray[i] = s;
    updatePos(i);
    //check cpatured stones:
    int[] neigh = neighbors(i);
    for(int j = 0; j<4;j++){
      //check liberties of neighbors
      if(opponents(s,boardArray[neigh[j]])){
        IntList captured = captSearch(neigh[j]);
        if(captured != null){
          //println("captured stones");
          
          for(int k=0;k<captured.size();k++){
            int d = captured.get(k);
            boardArray[d] = new Stone("empty");
            //float x = (d%(sz+2)) * unite;
            //float y = int(d/(sz+2))*unite;
            //fill(255,0,0);
            //ellipse(x,y,unite,unite);
          }
          //delay(2000);
        }
      }
    } 

  }
  int[] neighbors(int i){
    int[] neigh = {i-(size+2),i-1,i+1,i+(size+2)};
    return neigh;
  }
  
  IntList captSearch(int i){
    IntList group = new IntList();
    group.append(i);
    String[] boardStr = new String[boardArray.length];
    for(int j=0;j<boardArray.length;j++){
      boardStr[j] = boardArray[j].couleur;
    }
    IntList visiting = new IntList();
    visiting.append(i);
    String col = boardStr[i];
    boardStr[i] = "#";
    while(visiting.size()>0){
      int current = visiting.remove(0);
      int[] neighs = neighbors(current);
      for(int k=0;k<neighs.length;k++){
        int d = neighs[k];
        if(boardStr[d] == col){
          visiting.append(d);
          group.append(d);
          boardStr[d] = "#";
        }
        if(boardStr[d] == "empty"){
          
          return null;
        }
      }
    }
    //print("#group",group.size());
    //for(int z=0;z<group.size();z++){
    //  print(group.get(z));
    //}
    return group;
  }
  void updatePos(int i){
        int[] neigh = neighbors(i);
        int j;
        float xi = i%(size+2)*unitex + boardArray[i].sx;
        float yi = int(i/(size+2))*unitey + boardArray[i].sy;
        float xj,yj,vx,vy;
        float d,delta;
        Stone sj;
    for(int k = 0; k<neigh.length;k++){
      j = neigh[k];
      sj = boardArray[j];
      String colj = sj.couleur;
      if(colj=="black" || colj=="white"){
      xj = j%(size+2)*unitex + boardArray[j].sx;
      yj = int(j/(size+2))*unitey + boardArray[j].sy;
      d = distance(xi,yi,xj,yj);
      //println(xi,yi,xj,yj,d);
      if(d<2*rad){
        
        delta = (2*rad - d)+0.1;
        vx = (xj-xi)/d;
        vy = (yj-yi)/d;
        //println("d="+d+";rad="+rad+";delta="+delta+";vx="+vx+";vy="+vy);
        sj.move(delta*vx,delta*vy);
        updatePos(j);
      }
      }
    }
  }

    
}

float distance(float x1, float y1, float x2, float y2){
  return sqrt(sq(x1-x2)+sq(y1-y2));
}

String empty(int N){
   String emptyLine = " ";
   String plainLine = " ";
   String board = "";
   for(int i=0;i<N;i++){
     emptyLine += " ";
     plainLine += ".";
   }
   emptyLine += '\n';
   plainLine += '\n';
   board = emptyLine;
   for(int i=0;i<N;i++){
     board += plainLine;
   }
   board += emptyLine;
   return board;
}

void contact(int x, int y){
    
}

boolean opponents(Stone s1, Stone s2){
  return((s1.couleur == "black" && s2.couleur == "white")||(s1.couleur == "white" && s2.couleur == "black"));
}
