int[] offset = new int[2];
color noir = color(25,25,25);
color blanc = color(250,250,250);
int unitex;
int unitey;
float diam = 0.95;
float rad;
float fuz;
Node partey = parse2("(;FF[4]GM[1]SZ[19]CA[UTF-8]SO[gokifu.com]BC[cn]WC[cn]EV[]PB[Lian Xiao]BR[9p]PW[Mi Yuting]WR[9p]KM[7.5]DT[2018-10-19]RE[W+R]TM[150]LT[]TC[219]LC[5]GK[1];B[pd];W[dp];B[pq];W[dd];B[nc];W[qo];B[np];W[ql];B[cq];W[cp];B[dq];W[ep];B[fq];W[fp];B[gq];W[eq];B[er];W[fr];B[dr];W[hq];B[gr];W[gp];B[hr];W[dj];B[cc];W[cd];B[dc];W[fc];B[ec];W[ed];B[fb];W[gc];B[gb];W[hc];B[fe];W[dh];B[qj];W[md];B[mc];W[kd];B[ie];W[bc];B[bb];W[bd];B[hb];W[jc];B[kf];W[hg];B[hf];W[ig];B[je];W[ib];B[mf];W[nd];B[od];W[nf];B[eg];W[ff];B[ng];W[ge];B[ne];W[ik];B[ii];W[hi];B[dk];W[cj];B[gf];W[gg];B[hm];W[in];B[bn];W[bo];B[im];W[jm];B[hj];W[ij];B[gi];W[ji];B[hh];W[hk];B[gk];W[ih];B[fm];W[gl];B[fl];W[fk];B[gj];W[ek];B[dl];W[fi];B[hn];W[ho];B[jl];W[jn];B[il];W[of];B[ld];W[mg];B[lg];W[mh];B[ll];W[le];B[me];W[ke];B[ki];W[kj];B[li];W[pi];B[pl];W[pk];B[pj];W[oj];B[jh];W[hi];B[ok];W[qk];B[ii];W[jj];B[fg];W[gh];B[ef];W[fd];B[jf];W[nj];B[lj];W[qf];B[id];W[ic];B[cf];W[ab];B[ba];W[ha];B[fa];W[bf];B[bg];W[af];B[cg];W[lb];B[hl];W[hi];B[gm];W[fj];B[gl];W[fh];B[ip];W[iq];B[jp];W[jq];B[kp];W[hp];B[nh];W[mi];B[qi];W[og];B[re];W[rf];B[pm];W[qm];B[pn];W[qn];B[cn];W[kq];B[mq];W[lp];B[ko];W[lq];B[ln];W[no];B[oo];W[op];B[eh];W[kk];B[kl];W[nq];B[ei];W[kh];B[ag];W[ce];B[pp];W[mp];B[ej];W[rq];B[mb];W[lc];B[po];W[rd];B[rc];W[qd];B[qc];W[qe];B[or];W[an];B[am];W[ao];B[bl];W[qr];B[mr];W[nr];B[ns];W[bq];B[br];W[lr];B[ms];W[ar];B[lh];W[jg];B[nk];W[mj];B[mk];W[oh];B[bs];W[co])");
Node current;
  StringList pierresnoires = new StringList();
  StringList pierresblanches = new StringList();
String ident;
Board board;
int sz=19;
String pb,pw,br,wr,date,re;

String url = "http://gokifu.com/index.php";
String randomPage = "?p="+int(random(50));

String[] gamelist;
int gameNum = -1;
PFont font;
float eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ;
float textAngle;
float lightx,lighty,lightz;
float stoneHeight;
boolean stoploop;
PImage bg;
//PShape goban;

int today;

void setup(){
  today = day();
  println(url+randomPage);
  gamelist = gameFetch(url+randomPage);
  bg = loadImage("goban.jpg");
  size(600,600,P3D);
  font = createFont("Droid Sans Bold",40);
  textFont(font);
  sphereDetail(20);

  textSize(16);
  unitex = int(width/20);
  unitey = int(unitex*1.07);
  rad = diam*unitex/2+1;
  fuz = 1;//float(unite)/4;
  frameRate(2);
  lightx = -1;
  lighty = 0.5;
  lightz = -1;
  stoneHeight = unitex*diam/8;
  eyeX = width/2.0;
eyeY = height*1.5;
eyeZ = 1.2*(height/2.0) / tan(PI*30.0 / 180.0);
centerX = width/2.0;
centerY = height/2.0;
centerZ = 0;
upX = 0;
upY = 1;
upZ = 0;
textAngle = atan2(eyeZ-centerZ,eyeY-centerY)-PI/2;
  camera(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
  offset[0] = unitex;
  offset[1] = unitey;
  stoploop = false;
  //Node test = parse2("(;FF[4]RU[jap]DA[aujourd'hui]LO[dtc];B[aa];W[eg];B[az](;W[bb])(;W[cc]))");
  //test.disp("");
  //partey.disp("");
  nextGame();

  board = new Board(sz);
  //println(board);
  current = current.getFirstNext();
//  for(int i = 0; i<20; i++){
//  int x = 1+int(random(19));
//  int y = 1+int(random(19));
//  if(i%2==0){
//    drawStone(x,y,noir);
//  }
//  else{
//    drawStone(x,y,blanc);
//  }
//}
//for(int i = 0; i<20; i++){
//  int x = 1+int(random(19));
//  int y = 1+int(random(19));
//  if(i%2==0){
//    drawInfluence(x,y,noir,random(1));
//  }
//  else{
//    drawInfluence(x,y,blanc,random(1));
//  }
//}

}

void draw(){
  if(stoploop){
    delay(3000);
    stoploop=false;
  }
  ambientLight(102, 102, 102);
  float vx = width/2-mouseX;
  float vy = height/2-mouseY;
  float d = sqrt(sq(vx)+sq(vy));
  lightx = vx/d;
  lighty = vy/d;
  directionalLight(180, 180, 180, lightx,lighty,lightz);
  background(0,0,20);
  grid();
  info();
  updateBoard();
  drawBoard();
  if(current.isNexts()){
    current = current.getFirstNext();
  }
  else{
  pushMatrix();
  fill(255);
  translate(0,20*unitey,-120);
  rotateX(textAngle);
  //scale(0.8);
  text("Result: "+re,width/2-20,-50);
  popMatrix();
    
    
    stoploop = true;
    board = new Board(sz);
    nextGame();
  }
}

int[] letter2int(String coords){
  int[] output = new int[2];
  output[0] = int(coords.charAt(0))-int('a')+1;
  output[1] = sz-int(coords.charAt(1))+int('a');
  //println(coords,output[0],output[1]);
  return output;
}

void drawStone(float x,float y,color couleur){
  //stroke(2);
  //shadow
  //color opposite;
  // float r = red(couleur);
  // float g = green(couleur);
  // float b = blue(couleur);
  // opposite = color(255-r,255-g,255-b,50);
     noStroke();
   fill(0,90);
   float cx = x-lightx/lightz*stoneHeight;
   float cy = y-lighty/lightz*stoneHeight;
   pushMatrix();
   translate(0,0,2);
   ellipse(cx,cy,unitex*diam,unitey*diam);
  popMatrix();
  fill(couleur);
  pushMatrix();
  //ellipse(x,y,unite*diam,unite*diam);
  translate(x,y,stoneHeight);
  scale(1,1,0.5);
  shininess(1.0);
  sphere(unitex*diam/2);
  popMatrix();
  //shape(stone,x,y);
}

void drawInfluence(int x, int y, color couleur, float coef){
  noStroke();
  
  fill(couleur,int(255*coef));
  rectMode(CENTER);
  rect(x*unitex,y*unitey,unitex*coef*diam,unitey*coef*diam);
}

void drawBoard(){
  for(int i=1;i<sz+1;i++){
    for(int j=1;j<sz+1;j++){ 
      Stone s = board.get(i+(sz+2)*j);
      float x = i*unitex+s.sx;
      float y = j*unitey+s.sy;
      //text(str(i+j*(sz+2)),i*unite,j*unite);
      if(s.couleur == "black"){
        drawStone(x,y,noir);
      }
      if(s.couleur == "white"){
        drawStone(x,y,blanc);
      }
    }
  }
}

void updateBoard(){
 ArrayList<Property> props = current.getProps();
  String val;
  int[] coords;
  for(int i=0;i<props.size();i++){
    ident = props.get(i).getId();
    int cas = ("BWC").indexOf(ident);
    //println(ident,cas);
    switch(cas){
      case(0):
        //println("B");
        val = props.get(i).getVal()[0];
        coords = letter2int(val);
        board.add(coords[0]+(sz+2)*coords[1],new Stone("black")); 
        break;
      case(1):
        
        val = props.get(i).getVal()[0];
        coords = letter2int(val);
        board.add(coords[0]+(sz+2)*coords[1],new Stone("white")); 
        break;
      case(2):
        print("C:");
        val = props.get(i).getVal()[0];
        println(val);
    }
  } 
}

void grid(){
  lights();
  beginShape();
  texture(bg);
  vertex(offset[0]-unitex, offset[1]-unitey,0, 0, 0);
  vertex(offset[0]-unitex, offset[1]+sz*unitey, 0, 0,800);
  vertex(offset[0]+sz*unitex,offset[1]+sz*unitey,0,600,800);
  vertex(offset[0]+sz*unitex,offset[1]-unitey,0,600,0);
  endShape();
  stroke(0);
  strokeWeight(1);
  for(int i = 0;i<19;i++){
    fill(0);
    line(offset[0]+i*unitex,offset[1],offset[0]+i*unitex,offset[1]+18*unitey);
  }
  for(int j = 0;j<19;j++){
    fill(0);
    line(offset[0],offset[1]+j*unitey,offset[0]+18*unitex,offset[1]+j*unitey);
  }
  for(int i = 4;i<19;i=i+6){
    for(int j = 4;j<19;j=j+6){
      fill(0);
      ellipse(i*unitex,j*unitey,0.25*unitex,0.25*unitey);
    }
  }
      
}

void info(){
  pushMatrix();
  fill(255);
  translate(0,21*unitey,-120);
  rotateX(textAngle);
  fill(255);
  text(pb+" ("+br+")",40,-30);
  fill(255);
  text(pw+" ("+wr+")",40,-0);
  if(date!=null){
  text(date,width-100,-30);}
  scale(0.8);
  drawStone(20,-45,noir);
  drawStone(20,-10,blanc);
  popMatrix();
}



String put(String b, int index, char c){
  return b.substring(0,index-1)+c+b.substring(index);
}
  
String[] gameFetch(String url){
  String[] html = loadStrings(url);
  StringList gameList = new StringList();
  if(html == null)println("not loading..");
  for(int i=0;i<html.length;i++){
    String[] mhtml = match(html[i],"href=\"(http[^\"]*sgf)\"");
    if(mhtml!=null){
        println(mhtml[1]);
        gameList.append(mhtml[1]);
    }
  }
  return gameList.array();
}
  
void nextGame(){
  if (day()!=today){
    today = day();
    randomPage = "?p="+poissonRand(10);
    gamelist = gameFetch(url+randomPage);
    gameNum = -1;
  }
  gameNum = (gameNum+1)%gamelist.length;
  String sgf = join(loadStrings(gamelist[gameNum]),"");
  current = parse2(sgf).getFirstNext().getFirstNext();
  ArrayList<Property> props = current.getProps();
  //println(props.size());
  String val1;
  for(int i=0;i<props.size();i++){
    ident = props.get(i).getId();
    val1 = props.get(i).getVal()[0];
    //println(ident+" : "+val1);
    int pos = (".PBBRPWWRDTRE").indexOf(ident);
    switch(pos){
      case(1):
        pb = val1;
        break;
      case(3):
        br = val1;
      break;
      case(5):
        pw = val1;
      break;
      case(7):
        wr = val1;
      break;
      case(9):
        date = val1;
      break;
      case(11):
        re = val1;
    }
  }
  
}

int poissonRand(float lambda){
   float L = exp(-lambda);
   int k = 0;
   float p = 1;
   while(p>L){
     p=p*random(1);
     k++;
   }
   return k;
}